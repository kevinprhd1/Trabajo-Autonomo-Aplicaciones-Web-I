function validateLogin() {
  // Obtiene los valores de los campos de correo y contraseña
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;

  // Expresión regular para validar el formato de correo electrónico
  var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z.-]+\.[a-zA-Z]{2,4}$/;

  // Inicializa una variable para llevar un registro de los errores
  var error = false;

  // Obtiene elementos de error en el DOM para mostrar mensajes de error
  var emailError = document.getElementById("email-error");
  var passwordError = document.getElementById("password-error");

  // Limpia los mensajes de error previos
  emailError.innerHTML = "";
  passwordError.innerHTML = "";

  // Validación del campo de correo electrónico
  if (!email.match(emailPattern)) {
    error = true;
    emailError.innerHTML = "Correo Electrónico incorrecto.";
  }

  // Validación del campo de contraseña (mínimo 6 caracteres)
  if (password.length < 6) {
    error = true;
    passwordError.innerHTML = "Contraseña Incorrecta .";
  }

  if (error) {
    // Evita la redirección si hay errores en la validación
    return false;
  }

  // Realiza alguna otra acción después de la validación exitosa en el formulario de inicio de sesión
  // Por ejemplo, puedes mostrar un mensaje de éxito o enviar datos al servidor.

  return true; // Permite la redirección si no hay errores
}


function validateRegister() {
  // Obtiene los valores de los campos del formulario de registro
  var name = document.getElementById("name").value;
  var registerEmail = document.getElementById("register-email").value;
  var registerPassword = document.getElementById("register-password").value;

  // Expresión regular para validar el formato de correo electrónico
  var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

  // Inicializa una variable para llevar un registro de los errores
  var error = false;

  // Obtiene elementos de error en el DOM para mostrar mensajes de error
  var nameError = document.getElementById("name-error");
  var emailError = document.getElementById("register-email-error");
  var passwordError = document.getElementById("register-password-error");

  // Limpia los mensajes de error previos
  nameError.innerHTML = "";
  emailError.innerHTML = "";
  passwordError.innerHTML = "";

  // Validación del campo de nombre (debe tener contenido)
  if (name.trim() === "") {
    error = true;
    nameError.innerHTML = "Por favor, ingresa un nombre de usuario.";
  }

  // Validación del campo de correo electrónico
  if (!registerEmail.match(emailPattern)) {
    error = true;
    emailError.innerHTML = "El correo electrónico no es válido.";
  }

  // Validación del campo de contraseña (mínimo 5 caracteres)
  if (registerPassword.length < 5) {
    error = true;
    passwordError.innerHTML = "Contraseña muy corta (mínimo 5 caracteres).";
  }

  if (error) {
    // Evita la redirección si hay errores en la validación
    return false;
  }

  // Realiza alguna otra acción después de la validación exitosa en el formulario de registro
  // Por ejemplo, puedes mostrar un mensaje de éxito o enviar datos al servidor.

  window.location.href = "home.html";
  return true; // Permite la redirección si no hay errores
}

// Función para cerrar sesión
function cerrarSesion() {
    window.location.href = "index.html";
    console.log("Cerrando sesión...");
}

// Función para previsualizar la imagen seleccionada
function previewImage() {
    var input = document.getElementById('foto');
    var preview = document.getElementById('foto-preview');

    var file = input.files[0];

    if (file) {
        var reader = new FileReader();
        reader.onload = function(e) {
            preview.src = e.target.result;
        };
        reader.readAsDataURL(file);
    }
}

document.addEventListener('DOMContentLoaded', function () {
    const fileInput = document.getElementById('file-input');
    const uploadButton = document.getElementById('upload-button');
    const uploadedFilesList = document.getElementById('uploaded-files');
  
    // Al cargar la página, obtén los archivos almacenados en localStorage
    const storedFiles = JSON.parse(localStorage.getItem('uploadedFiles')) || [];
  
    // Función para eliminar un archivo de la lista y de localStorage
    function deleteFile(fileName, fileItem) {
      const index = storedFiles.indexOf(fileName);
      if (index !== -1) {
        storedFiles.splice(index, 1);
        localStorage.removeItem(fileName);
        uploadedFilesList.removeChild(fileItem);
        localStorage.setItem('uploadedFiles', JSON.stringify(storedFiles));
      }
    }
  
    // Agregar archivos almacenados previamente a la lista
    storedFiles.forEach(function (fileName) {
      const fileItem = document.createElement('li');
      
      // Crear un div para contener el nombre del archivo
      const fileNameText = document.createElement('span');
      fileNameText.textContent = fileName;
      
      // Agregar un botón de descarga
      const downloadButton = document.createElement('button');
      downloadButton.textContent = 'Descargar';
      downloadButton.addEventListener('click', function () {
        const downloadLink = document.createElement('a');
        downloadLink.href = 'data:text/plain,' + encodeURIComponent(localStorage.getItem(fileName));
        downloadLink.download = fileName;
        downloadLink.click();
      });
  
      // Agregar un botón de eliminación
      const deleteButton = document.createElement('button');
      deleteButton.textContent = 'Eliminar';
      deleteButton.addEventListener('click', function () {
        deleteFile(fileName, fileItem);
      });
      
      // Crear un elemento de salto de línea para separar los botones
      const lineBreak = document.createElement('br');
  
      fileItem.appendChild(fileNameText);
      fileItem.appendChild(lineBreak);
      fileItem.appendChild(downloadButton);
      fileItem.appendChild(deleteButton);
      uploadedFilesList.appendChild(fileItem);
    });
  
    // Manejar la subida de archivos
    uploadButton.addEventListener('click', function () {
      const file = fileInput.files[0];
      if (file) {
        const fileName = file.name;
        const fileItem = document.createElement('li');
  
        // Crear un div para contener el nombre del archivo
        const fileNameText = document.createElement('span');
        fileNameText.textContent = fileName;
  
        // Agregar un botón de descarga
        const downloadButton = document.createElement('button');
        downloadButton.textContent = 'Descargar';
        downloadButton.addEventListener('click', function () {
          const downloadLink = document.createElement('a');
          downloadLink.href = 'data:text/plain,' + encodeURIComponent(localStorage.getItem(fileName));
          downloadLink.download = fileName;
          downloadLink.click();
        });
  
        // Agregar un botón de eliminación
        const deleteButton = document.createElement('button');
        deleteButton.textContent = 'Eliminar';
        deleteButton.addEventListener('click', function () {
          deleteFile(fileName, fileItem);
        });
        
        // Crear un elemento de salto de línea para separar los botones
        const lineBreak = document.createElement('br');
  
        fileItem.appendChild(fileNameText);
        fileItem.appendChild(lineBreak);
        fileItem.appendChild(downloadButton);
        fileItem.appendChild(deleteButton);
        uploadedFilesList.appendChild(fileItem);
  
        // Guardar el archivo en localStorage
        storedFiles.push(fileName);
        localStorage.setItem(fileName, 'Contenido del archivo'); // Reemplaza 'Contenido del archivo' con el contenido real del archivo
        localStorage.setItem('uploadedFiles', JSON.stringify(storedFiles));
  
        // Limpiar el campo de entrada de archivo
        fileInput.value = '';
  
        // Puedes agregar aquí el código para subir el archivo al servidor si es necesario.
      }
    });
  });
